CREATE VIEW Widok_GrupaZajeciowa AS
	SELECT gz.Kod, gz.Dzien_tygodnia, gz.Parzystosc, gz.Godzina_rozpoczecia, gz.Godzina_zakonczenia, p.ID, p.Imie, p.Nazwisko, Kurs.Nazwa
	FROM Grupa_zajeciowa AS gz 
	INNER JOIN Prowadzacy AS p ON gz.ProwadzacyID = p.ID
	INNER JOIN Kurs ON gz.KursKod = Kurs.Kod;
	
CREATE VIEW Widok_GrupaProwadzacego AS
	SELECT gz.Kod, gz.Dzien_tygodnia, gz.Parzystosc, gz.Godzina_rozpoczecia, gz.Godzina_zakonczenia, p.ID, p.Imie, p.Nazwisko, Kurs.Nazwa
	FROM Grupa_zajeciowa AS gz 
	INNER JOIN Prowadzacy AS p ON gz.ProwadzacyID = p.ID
	INNER JOIN Kurs ON gz.KursKod = Kurs.Kod
	WHERE p.ID = CAST(SUBSTRING_INDEX(user(),'@',1) AS UNSIGNED);

CREATE VIEW Widok_Kurs AS
    SELECT Kod, Nazwa, Forma_kursu, ECTS, Forma_zaliczenia, Semestr
    FROM Kurs;
    
CREATE VIEW Widok_Ocena_Studenta AS
    SELECT Grupa_zajeciowaProwadzacyID, StudentNumer_indeksu, Grupa_zajeciowaKod, Wartosc, Uwagi, Data_wystawienia
    FROM Ocena
	WHERE StudentNumer_indeksu = CAST(SUBSTRING_INDEX(user(),'@',1) AS UNSIGNED);

CREATE VIEW Widok_Ocena_Prowadzacy AS
    SELECT Grupa_zajeciowaProwadzacyID, StudentNumer_indeksu, Grupa_zajeciowaKod, Wartosc, Uwagi, Data_wystawienia
    FROM Ocena
	WHERE Grupa_zajeciowaProwadzacyID = CAST(SUBSTRING_INDEX(user(),'@',1) AS UNSIGNED);

CREATE VIEW Widok_DaneProwadzacego AS
    SELECT ID, Imie, Nazwisko, Tytul, Adres_email
    FROM Prowadzacy;

CREATE VIEW Widok_Student AS
	SELECT s.Numer_indeksu, s.Imie, s.Nazwisko, k.Kierunek, w.Wydzial, s.Rok_rozpoczecia, s.Rok_studiow, s.Stopien_studiow, s.Semestr, a.Miejscowosc, a.Ulica, a.Numer_domu, a.Kod_pocztowy
	FROM Student AS s 
	INNER JOIN Kierunek AS k ON s.KierunekID = k.ID
	INNER JOIN Wydzial AS w on s.WydzialID = w.ID
	    INNER JOIN Student_Adres
	INNER JOIN Adres AS a ON s.Numer_indeksu = Student_Adres.StudentNumer_indeksu and a.ID = Student_Adres.AdresID
	WHERE s.Numer_Indeksu = CAST(SUBSTRING_INDEX(user(),'@',1) AS UNSIGNED);


CREATE VIEW Widok_Studenci AS
	SELECT s.Numer_indeksu, s.Imie, s.Nazwisko, k.Kierunek, w.Wydzial, s.Rok_rozpoczecia, s.Rok_studiow, s.Stopien_studiow, s.Semestr, a.Miejscowosc, a.Ulica, a.Numer_domu, a.Kod_pocztowy
	FROM Student AS s 
	INNER JOIN Kierunek AS k ON s.KierunekID = k.ID
	INNER JOIN Wydzial AS w on s.WydzialID = w.ID
	    INNER JOIN Student_Adres
	INNER JOIN Adres AS a ON s.Numer_indeksu = Student_Adres.StudentNumer_indeksu and a.ID = Student_Adres.AdresID;

