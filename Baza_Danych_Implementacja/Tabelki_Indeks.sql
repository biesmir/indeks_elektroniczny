CREATE TABLE Wydzial (
	ID INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	Wydzial NVARCHAR(60) NOT NULL 
);

CREATE TABLE Kierunek (
	ID INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	Kierunek NVARCHAR(60) NOT NULL 
);

CREATE TABLE Student (
	Numer_indeksu INT(9) NOT NULL AUTO_INCREMENT PRIMARY KEY, 
	Imie NVARCHAR(27) NOT NULL,
	Nazwisko NVARCHAR(50) NOT NULL,
	Rok_rozpoczecia INT(4) NOT NULL,
	Rok_studiow INT(1) NOT NULL,
	Stopien_studiow INT(1) NOT NULL,
	Semestr INT(1) NOT NULL,
	KierunekID INT(10),
	WydzialID INT(10),
	FOREIGN KEY (KierunekID) REFERENCES Kierunek(ID),
	FOREIGN KEY (WydzialID) REFERENCES Wydzial(ID),
	INDEX (Numer_indeksu, Imie, Nazwisko)
	
);

CREATE TABLE Adres (
	ID INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	Miejscowosc NVARCHAR(50) NOT NULL,
	Ulica NVARCHAR(50),
	Numer_domu VARCHAR(7) NOT NULL,
	Kod_pocztowy INT(5) NOT NULL
);

CREATE TABLE Student_Adres (
	ID INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	Rodzaj CHAR(1) NOT NULL,
	StudentNumer_indeksu INT(9),
	AdresID INT(10),
	FOREIGN KEY (StudentNumer_indeksu) REFERENCES Student(Numer_indeksu),
	FOREIGN KEY (AdresID) REFERENCES Adres(ID)
);

CREATE TABLE Prowadzacy (
	ID INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	Imie NVARCHAR(27) NOT NULL,
	Nazwisko NVARCHAR(50) NOT NULL,
	Tytul NVARCHAR(30) NOT NULL,
	Adres_email VARCHAR(100) NOT NULL,
	INDEX (ID, Imie, Nazwisko)
);

CREATE TABLE Kurs (
	Kod VARCHAR(10) NOT NULL UNIQUE PRIMARY KEY,
	Nazwa NVARCHAR(35) NOT NULL,
	Forma_kursu CHAR(1) NOT NULL,
	ECTS INT(2),
	Forma_zaliczenia CHAR(1) NOT NULL,
	Semestr INT(1),
	INDEX (Kod, Nazwa, Forma_kursu, Forma_zaliczenia)
);

CREATE TABLE Grupa_zajeciowa (
	Kod VARCHAR(10) NOT NULL UNIQUE PRIMARY KEY,
	Dzien_tygodnia CHAR(2),
	Parzystosc CHAR(2),
	Godzina_rozpoczecia TIME,
	Godzina_zakonczenia TIME,
	ProwadzacyID INT(10),
	KursKod VARCHAR(10),
	FOREIGN KEY (ProwadzacyID) REFERENCES Prowadzacy(ID),
	FOREIGN KEY (KursKod) REFERENCES Kurs(Kod),
	INDEX (Kod, Dzien_tygodnia, Parzystosc, Godzina_rozpoczecia, Godzina_zakonczenia)	
);

CREATE TABLE Grupa_zajeciowa_Student(
	ID INT(9) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	Grupa_zajeciowaKod VARCHAR(10),
	Grupa_zajeciowaProwadzacyID INT(10),
	KursKod VARCHAR(10),
	StudentNumer_indeksu INT(9),
	FOREIGN KEY (Grupa_zajeciowaKod) REFERENCES Grupa_zajeciowa(Kod),
	FOREIGN KEY (Grupa_zajeciowaProwadzacyID) REFERENCES Grupa_zajeciowa(ProwadzacyID),
	FOREIGN KEY (KursKod) REFERENCES Grupa_zajeciowa(KursKod),
	FOREIGN KEY (StudentNumer_indeksu) REFERENCES Student(Numer_indeksu)
);

CREATE TABLE Ocena (
	ID INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	Wartosc DECIMAL(2,1),
	Uwagi NVARCHAR(255),
	Data_wystawienia DATE,
	StudentNumer_indeksu INT(9),
	Grupa_zajeciowaKod VARCHAR(10),
	Grupa_zajeciowaProwadzacyID INT(10),
	FOREIGN KEY (StudentNumer_indeksu) REFERENCES Student(Numer_indeksu),
	FOREIGN KEY (Grupa_zajeciowaKod) REFERENCES Grupa_zajeciowa(Kod),
	FOREIGN KEY (Grupa_zajeciowaProwadzacyID) REFERENCES Grupa_zajeciowa(ProwadzacyID),
	INDEX (Wartosc)	
);

CREATE TABLE Ocena_czastkowa (
	ID INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	Wartosc REAL,
	Waga REAL,
	Uwagi NVARCHAR(255),
	Data_wystawienia DATE,
	OcenaID INT(10),
	StudentNumer_indeksu INT(9),
	Grupa_zajeciowaKod VARCHAR(10),
	Grupa_zajeciowaProwadzacyID INT(10),
	FOREIGN KEY (OcenaID) REFERENCES Ocena(ID),
	FOREIGN KEY (StudentNumer_indeksu) REFERENCES Ocena(StudentNumer_indeksu),
	FOREIGN KEY (Grupa_zajeciowaKod) REFERENCES Ocena(Grupa_zajeciowaKod),
	FOREIGN KEY (Grupa_zajeciowaProwadzacyID) REFERENCES Ocena(Grupa_zajeciowaProwadzacyID)
);

