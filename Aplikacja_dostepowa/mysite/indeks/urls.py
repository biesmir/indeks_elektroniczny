from django.urls import include, path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('modyfikacja/', views.modyfikacja, name='modyfikacja'),
    path('oceny/', views.oceny, name='oceny'),
    path('wiadomosci/', views.wiadomosci, name='wiadomosci'),
    path('twoje_grupy', views.grupy, name='twoje_grupy'),
    path('', include('django.contrib.auth.urls')),
]
