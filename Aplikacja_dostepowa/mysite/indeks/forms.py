from django.forms import ModelForm, Textarea
from . import models

class AdresForm(ModelForm):

    class Meta:
            model = models.Adres
            exclude = ('_id',)