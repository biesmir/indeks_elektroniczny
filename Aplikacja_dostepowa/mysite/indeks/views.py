from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib.auth import  authenticate, login
from django.contrib.auth.decorators import login_required
from .models import *
from . import forms

@login_required
def index(request):
    #Wyciagnac dane z bazy danych
    if request.user.is_superuser:
        return HttpResponseRedirect('/admin/')

    elif request.user.groups.values().first()['name'] == 'prowadzacy':
        prowadzacy_id = request.user.id
        prowadzacy_numer = StudentUser.objects.filter(user_id=prowadzacy_id).values().first()['numer_indeksu']
        prowadzacy_numer = -prowadzacy_numer
        prowadzacyDane = Prowadzacy.objects.filter(id=prowadzacy_numer)
        contex = {
            'ProwadzacyDane': prowadzacyDane,
        }

        return render(request, 'indeks/dane_prowadzacy.html', contex)

    else:
        student_id = request.user.id
        student_numer = StudentUser.objects.filter(user_id=student_id).values().first()['numer_indeksu']
        studentDane = Student.objects.filter(numer_indeksu=student_numer)
        studentKierunek = Kierunek.objects.filter(id=studentDane.values().first()['kierunekid_id'])
        studentWydzial = Wydzial.objects.filter(id=studentDane.values().first()['wydzialid_id'])
        adresStudent = StudentAdres.objects.filter(studentnumer_indeksu=student_numer)
        studentAdres = Adres.objects.filter(id=adresStudent.values().first()['adresid_id'])
        Student_Kierunek =  studentKierunek #'Robotyka'


        contex = {
            'Wydzial': studentWydzial,
            'Kierunek': Student_Kierunek,
            'Adres': studentAdres,
            'StudentDane': studentDane
        }
        return render(request, 'indeks/index.html',  contex)

@login_required
def modyfikacja(request):

    if request.user.is_superuser:
        return HttpResponseRedirect('/admin/')

    elif request.user.groups.values().first()['name'] == 'prowadzacy':
        return HttpResponseRedirect('/indeks/')

    else:
        if request.method == 'POST':

            formAdres = forms.AdresForm(request.POST)

            if formAdres.is_valid():
                student_id = request.user.id
                student_numer = StudentUser.objects.filter(user_id=student_id).values().first()['numer_indeksu']
                adresStudent = StudentAdres.objects.filter(studentnumer_indeksu=student_numer)
                studentAdres = Adres.objects.filter(id=adresStudent.values().first()['adresid_id'])
                miejscowosc = request.POST.get('miejscowosc')
                studentAdres.update(miejscowosc=miejscowosc)
                ulica = request.POST.get('ulica')
                studentAdres.update(ulica=ulica)
                numer_domu = request.POST.get('numer_domu')
                studentAdres.update(numer_domu=numer_domu)
                kod_pocztowy = request.POST.get('kod_pocztowy')
                studentAdres.update(kod_pocztowy=kod_pocztowy)

                return HttpResponseRedirect('/indeks/')

        else:

            formAdres = forms.AdresForm

        contex = {
            'formAdres': formAdres
        }

        return render(request, 'indeks/modyfikacja.html', contex)

@login_required
def oceny(request):

    if request.user.is_superuser:
        return HttpResponseRedirect('/admin/')

    elif request.user.groups.values().first()['name'] == 'prowadzacy':
        return HttpResponseRedirect('/indeks/')

    else:
        student_id = request.user.id
        student_numer = StudentUser.objects.filter(user_id=student_id).values().first()['numer_indeksu']
        studentOcena = Ocena.objects.filter(studentnumer_indeksu=student_numer)
        grupaZajeciowa = GrupaZajeciowa.objects
        kurs = Kurs.objects
        semestr = request.POST.get('Semestr')

        if(semestr is None):
            semestr = 1
        else:
            semestr = int(semestr)

        contex = {
            'Ocena': studentOcena,
            'Grupa': grupaZajeciowa,
            'Kurs': kurs,
            'Semestr': semestr,
        }

        return render(request, 'indeks/oceny.html', contex)


@login_required
def wiadomosci(request): 
    contex = {} 
    return render(request, 'indeks/wiadomosci.html', contex) 
 

@login_required
def grupy(request):
    if request.user.is_superuser:
        return HttpResponseRedirect('/admin/')

    elif request.user.groups.values().first()['name'] == 'prowadzacy':
        prowadzacy_id = request.user.id
        prowadzacy_numer = StudentUser.objects.filter(user_id=prowadzacy_id).values().first()['numer_indeksu']
        prowadzacy_numer = -prowadzacy_numer
        grupyDane = GrupaZajeciowa.objects.filter(prowadzacyid=prowadzacy_numer)
        kursyDane = Kurs.objects
        grupaStudent = GrupaZajeciowaStudent.objects.filter(grupa_zajeciowaprowadzacyid=prowadzacy_numer)
        studenci = Student.objects
        ocenyDane = Ocena.objects
        ocenyCzastkowe = OcenaCzastkowa.objects

        contex = {
            'Grupy' : grupyDane,
            'Kursy' : kursyDane,
            'GrupaStudent': grupaStudent,
            'Studenci': studenci,
            'Oceny': ocenyDane,
            'OcenyCzastkowe': ocenyCzastkowe,
        }
        return render(request, 'indeks/twoje_grupy.html', contex)

    else:
        return HttpResponseRedirect('/indeks/')


# Create your views here.
