# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save

class Adres(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    miejscowosc = models.CharField(db_column='Miejscowosc', max_length=50)  # Field name made lowercase.
    ulica = models.CharField(db_column='Ulica', max_length=50, blank=True, null=True)  # Field name made lowercase.
    numer_domu = models.CharField(db_column='Numer_domu', max_length=7)  # Field name made lowercase.
    kod_pocztowy = models.IntegerField(db_column='Kod_pocztowy')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Adres'


class GrupaStudent(models.Model):
    id = models.IntegerField(db_column='ID', primary_key=True)  # Field name made lowercase.
    grupa_zajeciowakod = models.TextField(db_column='Grupa_zajeciowaKod', blank=True, null=True)  # Field name made lowercase.
    grupa_zajeciowaprowadzacyid = models.IntegerField(db_column='Grupa_zajeciowaProwadzacyID', blank=True, null=True)  # Field name made lowercase.
    kurskod = models.TextField(db_column='KursKod', blank=True, null=True)  # Field name made lowercase.
    studentnumer_indeksu = models.IntegerField(db_column='StudentNumer_indeksu', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Grupa_student'


class GrupaZajeciowa(models.Model):
    kod = models.CharField(db_column='Kod', primary_key=True, max_length=10)  # Field name made lowercase.
    dzien_tygodnia = models.CharField(db_column='Dzien_tygodnia', max_length=2, blank=True, null=True)  # Field name made lowercase.
    parzystosc = models.CharField(db_column='Parzystosc', max_length=2, blank=True, null=True)  # Field name made lowercase.
    godzina_rozpoczecia = models.TimeField(db_column='Godzina_rozpoczecia', blank=True, null=True)  # Field name made lowercase.
    godzina_zakonczenia = models.TimeField(db_column='Godzina_zakonczenia', blank=True, null=True)  # Field name made lowercase.
    prowadzacyid = models.ForeignKey('Prowadzacy', models.DO_NOTHING, db_column='ProwadzacyID', blank=True, null=True)  # Field name made lowercase.
    kurskod = models.ForeignKey('Kurs', models.DO_NOTHING, db_column='KursKod', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Grupa_zajeciowa'


class GrupaZajeciowaStudent(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    grupa_zajeciowakod = models.ForeignKey(GrupaZajeciowa, models.DO_NOTHING, db_column='Grupa_zajeciowaKod', blank=True, null=True)  # Field name made lowercase.
    grupa_zajeciowaprowadzacyid = models.ForeignKey(GrupaZajeciowa, models.DO_NOTHING, db_column='Grupa_zajeciowaProwadzacyID', blank=True, null=True,related_name='GrupaZajeciowa_grupa_zajeciowaprowadzacyid')  # Field name made lowercase.
    kurskod = models.ForeignKey(GrupaZajeciowa, models.DO_NOTHING, db_column='KursKod', blank=True, null=True,related_name='GrupaZajeciowa_kurskod')  # Field name made lowercase.
    studentnumer_indeksu = models.ForeignKey('Student', models.DO_NOTHING, db_column='StudentNumer_indeksu', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Grupa_zajeciowa_Student'
        
class Kierunek(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    kierunek = models.CharField(db_column='Kierunek', max_length=60)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Kierunek'


class Kurs(models.Model):
    kod = models.CharField(db_column='Kod', primary_key=True, max_length=10)  # Field name made lowercase.
    nazwa = models.CharField(db_column='Nazwa', max_length=35)  # Field name made lowercase.
    forma_kursu = models.CharField(db_column='Forma_kursu', max_length=1)  # Field name made lowercase.
    ects = models.IntegerField(db_column='ECTS', blank=True, null=True)  # Field name made lowercase.
    forma_zaliczenia = models.CharField(db_column='Forma_zaliczenia', max_length=1)  # Field name made lowercase.
    semestr = models.IntegerField(db_column='Semestr', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Kurs'


class Ocena(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    wartosc = models.DecimalField(db_column='Wartosc', max_digits=2, decimal_places=1, blank=True, null=True)  # Field name made lowercase.
    uwagi = models.CharField(db_column='Uwagi', max_length=255, blank=True, null=True)  # Field name made lowercase.
    data_wystawienia = models.DateField(db_column='Data_wystawienia', blank=True, null=True)  # Field name made lowercase.
    studentnumer_indeksu = models.ForeignKey('Student', models.DO_NOTHING, db_column='StudentNumer_indeksu', blank=True, null=True)  # Field name made lowercase.
    grupa_zajeciowakod = models.ForeignKey(GrupaZajeciowa, models.DO_NOTHING, db_column='Grupa_zajeciowaKod', blank=True, null=True, related_name='GrupaZajeciowa_grupa_zajeciowakod')  # Field name made lowercase.
    grupa_zajeciowaprowadzacyid = models.ForeignKey(GrupaZajeciowa, models.DO_NOTHING, db_column='Grupa_zajeciowaProwadzacyID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Ocena'


class OcenaCzastkowa(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    wartosc = models.FloatField(db_column='Wartosc', blank=True, null=True)  # Field name made lowercase.
    waga = models.FloatField(db_column='Waga', blank=True, null=True)  # Field name made lowercase.
    uwagi = models.CharField(db_column='Uwagi', max_length=255, blank=True, null=True)  # Field name made lowercase.
    data_wystawienia = models.DateField(db_column='Data_wystawienia', blank=True, null=True)  # Field name made lowercase.
    ocenaid = models.ForeignKey(Ocena, models.DO_NOTHING, db_column='OcenaID', blank=True, null=True,related_name='Ocena_grupa_ocenaid')  # Field name made lowercase.
    studentnumer_indeksu = models.ForeignKey(Ocena, models.DO_NOTHING, db_column='StudentNumer_indeksu', blank=True, null=True,related_name='Ocena_grupa_studentnumer_indeksu')  # Field name made lowercase.
    grupa_zajeciowakod = models.ForeignKey(Ocena, models.DO_NOTHING, db_column='Grupa_zajeciowaKod', blank=True, null=True,related_name='Ocena_grupa_grupa_zajeciowakod')  # Field name made lowercase.
    grupa_zajeciowaprowadzacyid = models.ForeignKey(Ocena, models.DO_NOTHING, db_column='Grupa_zajeciowaProwadzacyID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Ocena_czastkowa'


class Prowadzacy(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    imie = models.CharField(db_column='Imie', max_length=27)  # Field name made lowercase.
    nazwisko = models.CharField(db_column='Nazwisko', max_length=50)  # Field name made lowercase.
    tytul = models.CharField(db_column='Tytul', max_length=30)  # Field name made lowercase.
    adres_email = models.CharField(db_column='Adres_email', max_length=100)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Prowadzacy'


class Student(models.Model):
    numer_indeksu = models.AutoField(db_column='Numer_indeksu', primary_key=True)  # Field name made lowercase.
    imie = models.CharField(db_column='Imie', max_length=27)  # Field name made lowercase.
    nazwisko = models.CharField(db_column='Nazwisko', max_length=50)  # Field name made lowercase.
    rok_rozpoczecia = models.IntegerField(db_column='Rok_rozpoczecia')  # Field name made lowercase.
    rok_studiow = models.IntegerField(db_column='Rok_studiow')  # Field name made lowercase.
    stopien_studiow = models.IntegerField(db_column='Stopien_studiow')  # Field name made lowercase.
    semestr = models.IntegerField(db_column='Semestr')  # Field name made lowercase.
    kierunekid = models.ForeignKey(Kierunek, models.DO_NOTHING, db_column='KierunekID', blank=True, null=True)  # Field name made lowercase.
    wydzialid = models.ForeignKey('Wydzial', models.DO_NOTHING, db_column='WydzialID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Student'


class StudentAdres(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    rodzaj = models.CharField(db_column='Rodzaj', max_length=1)  # Field name made lowercase.
    studentnumer_indeksu = models.ForeignKey(Student, models.DO_NOTHING, db_column='StudentNumer_indeksu', blank=True, null=True)  # Field name made lowercase.
    adresid = models.ForeignKey(Adres, models.DO_NOTHING, db_column='AdresID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Student_Adres'


class Wydzial(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    wydzial = models.CharField(db_column='Wydzial', max_length=60)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Wydzial'


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class IndeksAdres(models.Model):
    field_id = models.AutoField(db_column='_id', primary_key=True)  # Field renamed because it started with '_'.
    miejscowosc = models.CharField(db_column='Miejscowosc', max_length=50)  # Field name made lowercase.
    ulica = models.CharField(db_column='Ulica', max_length=50)  # Field name made lowercase.
    numer_domu = models.CharField(db_column='Numer_domu', max_length=7)  # Field name made lowercase.
    kod_pocztowy = models.IntegerField(db_column='Kod_pocztowy')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'indeks_adres'


class IndeksKierunek(models.Model):
    field_id = models.AutoField(db_column='_id', primary_key=True)  # Field renamed because it started with '_'.
    kierunek = models.CharField(db_column='Kierunek', max_length=60)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'indeks_kierunek'


class IndeksStudent(models.Model):
    numer_indeksu = models.IntegerField(db_column='Numer_indeksu', primary_key=True)  # Field name made lowercase.
    imie = models.CharField(db_column='Imie', max_length=27)  # Field name made lowercase.
    nazwisko = models.CharField(db_column='Nazwisko', max_length=50)  # Field name made lowercase.
    rok_rozpoczecia = models.IntegerField(db_column='Rok_rozpoczecia')  # Field name made lowercase.
    rok_studiow = models.IntegerField(db_column='Rok_studiow')  # Field name made lowercase.
    stopien_studiow = models.IntegerField(db_column='Stopien_studiow')  # Field name made lowercase.
    semestr = models.IntegerField(db_column='Semestr')  # Field name made lowercase.
    kierunekid = models.ForeignKey(IndeksKierunek, models.DO_NOTHING, db_column='KierunekID_id')  # Field name made lowercase.
    wydzialid = models.ForeignKey('IndeksWydzial', models.DO_NOTHING, db_column='WydzialID_id')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'indeks_student'


class IndeksWydzial(models.Model):
    field_id = models.AutoField(db_column='_id', primary_key=True)  # Field renamed because it started with '_'.
    wydzial = models.CharField(db_column='Wydzial', max_length=60)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'indeks_wydzial'


class StudentUser(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	numer_indeksu = models.IntegerField( unique='True', null='False')

	class Meta:
		managed = True
		db_table = 'StudentUser'

def create_student_user(sender, instance, created, **kwargs):
	if created:
		profile, created = StudentUser.objects.get_or_create(user=instance)

post_save.connect(create_student_user, sender=User)
