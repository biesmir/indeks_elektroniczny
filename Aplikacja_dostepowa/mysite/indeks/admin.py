from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User

from .models import *

admin.site.register(Kierunek)
admin.site.register(Wydzial)
admin.site.register(Student)
admin.site.register(Adres)

class StudentUserInline(admin.StackedInline):
    model = StudentUser
    can_delete = False
    verbose_name_plural = 'StudentUser'

class UserAdmin(BaseUserAdmin):
    inlines = (StudentUserInline,)

admin.site.unregister(User)
admin.site.register(User, UserAdmin)


# Register your models here.
